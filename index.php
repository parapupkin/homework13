<?php

$host = 'localhost';
$dbname = 'First';
$user = 'root';
$pass = '';

try {
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $pass);

    if (isset($_GET['selection'])) {
        if ($_GET['selection'] == 'date') {
        $sql = $db->query("SELECT * FROM `tasks` ORDER BY `tasks`.`date_added` ASC");       
        } elseif ($_GET['selection'] == 'status') {
            $sql = $db->query("SELECT * FROM `tasks` ORDER BY `tasks`.`is_done` DESC");        
        } elseif ($_GET['selection'] == 'description') {
            $sql = $db->query("SELECT * FROM `tasks` ORDER BY `tasks`.`description` ASC");        
        } else {
            echo "Не майтесь дурью";
            exit();
        }
    } else {
        $sql = $db->query("SELECT * FROM tasks");
    }     

    if (!empty($_GET['id'])) {
        $id = $_GET['id'];
    }  

    $name_insert = "insert"; 
   
    if (isset($_GET['action'])) {
            // удаление записи
        if ($_GET['action'] == 'delete') {               
            $sql_delete = $db->prepare("DELETE FROM tasks WHERE `tasks`.`id` = :id");
            $sql_delete->bindValue(':id', $id, PDO::PARAM_INT);
            $sql_delete->execute();
            header("Location: index.php");
        }

        // пометка записи, как выполненной
        if ($_GET['action'] == 'done') {                  
            $sql_done = $db->prepare("UPDATE `tasks` SET `is_done` = '1' WHERE `tasks`.`id` = :id");
            $sql_done->bindValue(':id', $id, PDO::PARAM_INT);
            $sql_done->execute();
            header("Location: index.php");
        }

        // редактирование записи
        if ($_GET['action'] == 'edite') {        
            $sql_edite = $db->prepare("SELECT * FROM tasks WHERE `tasks`.`id` = :id");
            $sql_edite->bindValue(':id', $id, PDO::PARAM_INT);
            $sql_edite->execute();
            $value = $sql_edite->fetch();
            $name_insert = "update";
        }       
    }

    // вставка записи
    if (!empty($_GET['insert'])) {        
        $sql_insert = $db->prepare("INSERT INTO `tasks` (description, date_added) VALUES (:value, NOW())");
        $sql_insert->bindValue(':value', $_GET['insert'], PDO::PARAM_STR);
        $sql_insert->execute();
        header("Location: index.php");
    } 

    // обновление отредактированной записи
    if (!empty($_GET['update'])) {            
        $sql_update = $db->prepare("UPDATE `tasks` SET `description` = :description WHERE `tasks`.`id` = :id");        
        $sql_update->execute([
            ':id' => $id,
            ':description' => $_GET['update']
        ]);        
        header("Location: index.php");
    }    

} catch (Exception $e) {
    die('Error: ' . $e->getMessage() . '<br/>');
}        
    
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style type="text/css">
        table {
            border-collapse: collapse;            
        }
        td, th {
            border: 1px solid grey;
            padding: 5px;
        }
        th {
            background-color: #EEEEEE;
        }
        input {
            margin-bottom: 20px;
        }

        form {
            display: inline-block;            
        }
        a {
            margin-right: 5px;
        }

    </style>
</head>
<body>
    <h1>Список дел на сегодня</h1>    
    <form method="get">
        <input name="<?= $name_insert ?>" placeholder="Введите описание дела" value="<?= $value['description'] ?>">  
        <input type="hidden" name="id" value="<?= $id ?>">       
        <input type="submit" name="" value="Сохранить">
    </form>    
    <form id="choice">Отсортировать по:
    </form method="get">
        <select name="selection" form="choice">
            <option value="date">Дате добавления</option>
            <option value="status">Статусу</option>
            <option value="description">Описанию</option>    
        </select>
    <input type="submit" form="choice" value="Отсортировать">
    <table>
        <tr>
            <th>Описание задачи</th>
            <th>Дата добавления </th>
            <th>Статус</th>
            <th>Изменить/Выполнить/Удалить</th>            
        </tr>
        <?php while ($date = $sql->fetch()) { 
            if ($date['is_done'] == 0) {
                $status = "В процессе";
                $color = "orange";
            } else {
                $status = "Выполнено";
                $color = "green";
            } ?>  
        <tr>              
            <td><?php echo $date['description'] ?></td>
            <td><?php echo $date['date_added']?></td>
            <td style="color: <?= $color?>"><?php echo $status?></td>            
            <td><a href="?id=<?= $date['id'] ?>&action=edite">Изменить</a><a href="?id=<?= $date['id'] ?>&action=done">Выполнить</a><a href="?id=<?= $date['id'] ?>&action=delete">Удалить</a></td>                         
        </tr>
        <?php } ?>
    </table>
    
</body>
</html>